class DemoController < ApplicationController

  layout 'application'

  def index
  end

  def hello
    @array = [*1..5]
    @id = params['id']
    @page = params[:page].to_i
  end

  def other_hello
    redirect_to( :action => 'index')
  end

  def text_helpers

  end

end
